var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req, res){
  //res.send('Hola Mundo');
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.post('/', function(req,res){
  res.send('Hemos recibido su petición');
});

app.get('/clientes/', function(req, res){
  //res.send('Hola Mundo');
  res.sendFile(path.join(__dirname, 'clientes.html'));
});

var clientes = [{"user":"user 0", "pass":"pass 0"},
                {"user":"user 1", "pass":"pass 1"}];

//con paso de parámetros
app.get('/clientes/:idcliente', function(req, res){
  //res.send('Hola Mundo');
  var id = req.params.idcliente;
  if (id<2){
    res.send('Usuario: ' + clientes[id].user + ' Pass: ' + clientes[id].pass);
  } else {
    res.send('Introduce cliente 0 o 1');
  }
});


app.post('/clientes/', function(req,res){
  //res.sendFile(path.join(__dirname, 'post.json'));
  res.send([{"usuario 1":"json de usuario 1 recepción petición inserción clientes"},
            {"usuario 2":"json de usuario 2 recepción petición inserción de clientes"}]);
});

app.put('/clientes/', function(req,res){
  res.sendFile(path.join(__dirname, 'put.json'));
});

app.delete('/clientes/', function(req,res){
  res.sendFile(path.join(__dirname, 'delete.json'));
});
